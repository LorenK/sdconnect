import React from 'react'
import { StackNavigator } from 'react-navigation'

import Splash from "./src/views/splash"
import Login from "./src/views/login"
import Setup from "./src/views/setup"
import Home from "./src/views/home"
import TeacherHome from "./src/views/teacherHome"
import FullPost from "./src/views/fullPost"
import Search from "./src/views/search"
import ProfileImage from "./src/views/profileImage"

const OVERRIDE = Splash
const App = StackNavigator({
	Splash: {
		screen: OVERRIDE,
		navigationOptions: {
			header : null
		}
	},
	Login: {
		screen: Login,
		navigationOptions: {
			header : null
		}
	},
	Setup: {
		screen: Setup,
		navigationOptions: {
			header : null
		}
	},
	Home: {
		screen: Home,
		navigationOptions: {
			header : null
		}
	},
	TeacherHome: {
		screen: TeacherHome,
		navigationOptions: {
			header : null
		}
	},
	FullPost: {
		screen: FullPost,
		navigationOptions: {
			header : null
		}
	},
	Search: {
		screen: Search,
		navigationOptions: {
			header : null
		}
	},
	ProfileImage: {
		screen: ProfileImage,
		navigationOptions: {
			header : null
		}
	}
}, { headerMode: 'screen', mode: 'modal' })
export default App