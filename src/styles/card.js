import { StyleSheet } from 'react-native'
import colors from '../constants/colors'

const cardPadding = 18
const cardMargin = 24
export default StyleSheet.create({
    container: {
        backgroundColor: colors.textPrimary,
        padding: cardPadding,
        marginBottom: cardMargin
    },
    profilePhoto: {
        borderRadius: 25,
        width: 50,
        height: 50,
    },
    profilePhotoSm: {
        borderRadius: 15,
        width: 30,
        height: 30,
    },    
    profileEmpty: {
        backgroundColor: colors.secondary,
        width: 50,
        height: 50,
        borderRadius: 50,
        justifyContent: "center",
        alignItems: "center"
    },
    actionText: {
        color: colors.primary,
    },
    head: {
        color: colors.textPrimary,
        textAlign: 'center'
    },
    row: {
        flexDirection: 'row'
    },
    center: {
        justifyContent: 'center',
        alignSelf: "center",
        alignItems: "center"
    },
    floatRight: {
        flex: 1,
        alignItems: 'flex-end'
    },
    textBody: {
        color: colors.textSecondary
    },
    scheduleCard: {
        flex: 1,
        padding: cardPadding,
        // marginLeft: cardMargin, 
        // marginRight: cardMargin,
        backgroundColor: colors.primary
    }
})