import { StyleSheet } from 'react-native'
import fonts from '../constants/fonts'

export default StyleSheet.create({
    h1: {
        fontFamily: fonts.georgia,
        fontSize: 42
    },
    h1b: {
        fontFamily: fonts.georgiaBold,
        fontSize: 42
    },
    scheduleHead: {
        fontFamily: fonts.arialBold,
        fontSize: 32
    },
    scheduleSubhead: {
        fontFamily: fonts.arial,
        fontSize: 20
    },
    h2: {
        fontFamily: fonts.arial,
        fontSize: 16
    },
    h2b: {
        fontFamily: fonts.arialBold,
        fontSize: 16
    },
    p: {
        fontFamily: fonts.arial
    },
    pb: {
        fontFamily: fonts.arialBold
    },
    sm: {
        fontFamily: fonts.arial,
        fontSize: 12
    },
    smb: {
        fontFamily: fonts.arialBold,
        fontSize: 12
    },
    xsmi: {
        fontFamily: fonts.arialItalic,
        fontSize: 10
    },
    input: {
        fontFamily: fonts.arial,
        fontSize: 16,
    },
    button: {
        fontFamily: fonts.arialBold,
        fontSize: 12
    },
    error: {
        fontFamily: fonts.arial,
        fontSize: 10,
        color: "#FB2626" // Consts?
    }
})