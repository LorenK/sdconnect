import { StyleSheet, Platform } from 'react-native'
import colors from '../constants/colors'

const common = {
    container: {
        flex: 1,
        backgroundColor: colors.background,
    },
    topBar: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: "center",
    },
    topBarItem: {
        flex: 1,
        textAlign: "center",
        paddingTop: Platform.OS === 'android' ? 22 : 42,
        padding: 22
    },
    topBarItemNoText: {
        flex: 1,
        padding: 22
    },
    left: {
        textAlign: "left",
        paddingLeft: 16
    },
    right: {
        textAlign: "right",
        paddingRight: 16
    },
    center: {
        textAlign: "center"
    },
}

export const plainStyles = StyleSheet.create({
    container: common.container,
    topBar: common.topBar,
    topBarItem: common.topBarItem,
    topBarItemNoText: common.topBarItemNoText,
    left: common.left,
    right: common.right,
    center: common.center,
    topBarColor: {
        backgroundColor: colors.textPrimary
    },
    topBarItemColor: {
        color: colors.primary
    }
})

export const loginStyles = StyleSheet.create({
    container: common.container,
    topBar: common.topBar,
    topBarItem: common.topBarItem,
    topBarItemNoText: common.topBarItemNoText,
    left: common.left,
    right: common.right,
    center: common.center,
    topBarColor: {
        backgroundColor: colors.primary
    },
    topBarItemColor: {
        color: colors.textPrimary
    },
    buttonView: {
        flexDirection: 'row',
        justifyContent: "center",
        alignSelf: "center",
        alignItems: "center"
    }
})

export const contentStyles = StyleSheet.create({
    container: common.container,
    topBar: common.topBar,
    topBarItem: common.topBarItem,
    topBarItemNoText: common.topBarItemNoText,
    left: common.left,
    right: common.right,
    center: common.center,
    topBarColor: {
        backgroundColor: colors.background
    },
    topBarItemColor: {
        color: colors.primary
    },
    cardList: {
        paddingLeft: 16,
        paddingRight: 16,
        paddingBottom: 16
    },
    contentMargins: {
        marginLeft: 16,
        marginRight: 16
    },
})