import { StyleSheet } from 'react-native'
import colors from '../constants/colors'
import fonts from '../constants/fonts'

const padding = 12
export default StyleSheet.create({
    baseInput: {
        backgroundColor: colors.textPrimary,
        borderColor: colors.secondary,
        borderWidth: 2,
        color: colors.primary,
        margin: 0,
        paddingLeft: padding,
        paddingRight: padding,
        paddingTop: padding - 4,
        paddingBottom: padding - 4
    },
    baseInputFocused: {
        backgroundColor: colors.textPrimary,
        borderColor: colors.primary,
        borderWidth: 2,
        color: colors.primary,
        margin: 0,
        paddingLeft: padding,
        paddingRight: padding,
        paddingTop: padding - 4,
        paddingBottom: padding - 4
    },
    label: {
        color: colors.primary,
        paddingBottom: 2
    },
    clearInput: {
        backgroundColor: colors.textPrimary,
        color: colors.primary,
        margin: 0,
        paddingLeft: padding,
        paddingRight: padding,
        paddingTop: padding - 4,
        paddingBottom: padding - 4
    }
})