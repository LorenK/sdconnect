import { StyleSheet } from 'react-native'
import colors from '../constants/colors'

export default StyleSheet.create({
    container: {
        backgroundColor: colors.primary,
        flexDirection: 'row',
        padding: 16
    },
    pagerItem: {
        flex: 1,
        textAlign: "center"
    },
})