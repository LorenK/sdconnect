import { StyleSheet } from 'react-native'
import colors from '../constants/colors'

const innerPadding = 12
export const buttonStyles = StyleSheet.create({
    fill: {
        justifyContent: "center",
        alignSelf: "center",
        alignItems: "center",
        backgroundColor: colors.primary,
        borderRadius: 6,
        padding: innerPadding,
    },
    outline: {
        justifyContent: "center",
        alignSelf: "center",
        alignItems: "center",
        borderStyle: "solid",
        borderWidth: 3,
        borderColor: colors.secondary,
        backgroundColor: colors.textPrimary,
        borderRadius: 6,
        padding: innerPadding - 1
    },
    text: {
        textAlign: 'center',
        color: colors.textPrimary
    },
    textNoFill: {
        textAlign: 'center',
        color: colors.primary
    },
    icon: {
        textAlign: 'center',
        color: colors.textPrimary
    },
    iconNoFill: {
        textAlign: 'center',
        color: colors.primary
    }
})

export const cardButtonStyles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        padding: 24,
        marginBottom: 24,
        backgroundColor: colors.textPrimary
    },
    floatRight: {
        flex: 1,
        alignItems: 'flex-end'
    },
    text: {
        color: colors.primary
    },
    textSecondary: {
        color: colors.secondary
    }
})