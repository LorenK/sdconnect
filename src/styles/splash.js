import { StyleSheet } from 'react-native'
import colors from '../constants/colors'

export default StyleSheet.create({
    image: {
        height: "30%",
        marginTop: "20%",
        marginBottom: "10%",
        justifyContent: "center",
        alignSelf: "center",
        alignItems: "center",
    },
    header: {
        color: colors.primary,
        textAlign: "center",
        paddingLeft: 50,
        paddingRight: 50,
        paddingBottom: 20
    },
    subheader: {
        color: colors.primary,
        textAlign: "center",
        paddingLeft: 50,
        paddingRight: 50
    },
    buttonView: {
        flexDirection: 'row',
        justifyContent: "center",
        alignSelf: "center",
        alignItems: "center",
        margin: 18
    }
})