import firebase from 'react-native-firebase'
import accountType from '../constants/accountType'

import { refs, Actions, Context } from './store'
// Use Redux to store refs, cached data, ect, this will likely solve our data persistance among screens problem

class Ref {
    constructor(path) {
        this.path = path
    }

    doc() {
        return firebase.firestore().doc(this.path)        
    }

    toString() {
        return this.path
    }
}

class SchoolRef extends Ref {
    constructor(id) {
        super(`schools/${id}`)
        this.id = id
    }

    classes() {
        return firebase.firestore().collection(`${this.path}/classes`)
    }

    class(classId) {
        const ref = new ClassRef(`${this.path}/classes/${classId}`)
        ref.id = classId
        return ref
    }

    users() {
        return firebase.firestore().collection(`${this.path}/users`)
    }

    user(userId) {
        const ref = new Ref(`${this.path}/users/${userId}`)
        ref.id = userId
        return ref
    }
}

class ClassRef extends Ref {
    posts() {
        return firebase.firestore().collection(`${this.path}/posts`)
    }

    post(postId) {
        const ref = new PostRef(`${this.path}/posts/${postId}`)
        ref.id = postId
        return ref
    }
}

class PostRef extends Ref {
    comments() {
        return firebase.firestore().collection(`${this.path}/comments`)
    }

    comment(commentId) {
        const ref = new Ref(`${this.path}/comments/${commentId}`)
        ref.id = commentId
        return ref
    }
}

export class ContextController {
    static setUser(user) { // Actual user obj
        refs.dispatch({type: Actions.ADD, context: Context.USER, value: user})
    }

    static setSchool(school) {
        refs.dispatch({type: Actions.ADD, context: Context.SCHOOL, value: new SchoolRef(school)})
    }

    static setClass(classId) {
        // get school
        const school = refs.getState().school
        refs.dispatch({ type: Actions.ADD, context: Context.CLASS, value: school.class(classId) })
    }

    static setPost(postId) {
        const _class = refs.getState().class
        refs.dispatch({ type: Actions.ADD, context: Context.POST, value: _class.post(postId) })
    }

    static setComment(commentId) {
        const post = refs.getState().post
        refs.dispatch({ type: Actions.ADD, context: Context.COMMENT, value: post.comment(commentId) })
    }
}

// Abstracted Firebase lib, allows contextual calls, only ever 1
export class ContextualFirebase {
    constructor() {
        // Called whenever we change the Redux state
        refs.subscribe(() => {
            const state = refs.getState()
            
            this.me = state.user
            this.school = state.school
            this.currentClass = state.class ? state.class.doc() : undefined
            this.currentPost = state.post ? state.post.doc() : undefined
            this.currentComment = state.comment ? state.comment.doc() : undefined

            if (this.me && this.school) {
                if (!this.meRef) {
                    this.meRef = this.school.user(this.me.uid).doc()
                    this.subscribeToNotifications()
                }
            }
        })
    }

    setMe(user) { this.me = user }
    setMeRef(userRef) { this.meRef = userRef }
    setSchool(school) { this.school = school }
    setCurrentClass(_class) { this.currentClass = _class }
    setCurrentPost(post) { this.currentPost = post }
    setCurrentComment(comment) { this.currentComment = comment }

    // Unsubscribes us from the Redux
    unsubscribe() {
        refs.unsubscribe()
    }

    subscribeToNotifications() {
        Firebase.getClassNames(this.meRef).then(classNames => {
            for (var i = 0; i < classNames.length; i++) {
                const className = classNames[i]
                firebase.messaging().subscribeToTopic(`${this.school.id}_${className}`)
            }
        })
    }

    // Not to be re-used or stored in app state,
    /// This should be called fresh every time to ensure we have updated user data
    getMyData() {
        return Firebase.getUserData(this.meRef)
    }

    setMyData(userData) {
        return Firebase.setUserData(this.meRef, userData)
    }

    getAccountType() {
        return Firebase.getAccountType(this.meRef)
    }

    uploadFile(path, meta) {
        return Firebase.uploadFile(this.me, path, meta)
    }

    uploadFileGetLinkAndUser(path, meta) {
        return Firebase.uploadFileGetLinkAndUser(this.me, path, meta)
    }

    updateUsername(firstName, lastName) {
        return Firebase.updateUsername(this.me, firstName, lastName)
    }

    updateProfileImage(profilePhoto) {
        return Firebase.updateProfileImage(this.me, profilePhoto)
    }

    completeAccountSetup(email, password) {
        return Firebase.completeAccountSetup(this.me, this.meRef, email, password)
    }

    /// Classes
    classNames() {
        return Firebase.getClassNames(this.meRef)
    }

    classes() {
        return Firebase.getClasses(this.meRef)
    }

    class() {
        return Firebase.getClass(this.currentClass)
    }

    /// Posts
    posts(classRef) {
        if (classRef)
            return Firebase.getPosts(classRef, this.meRef)
        return Firebase.getPosts(this.currentClass, this.meRef)
    }

    post() {
        return Firebase.getPost(this.currentPost, this.meRef)
    }

    newPost(text, notify) {
        return Firebase.newPost(this.currentClass, text, notify)
    }
    
    likePost(postRef) {
        if (postRef)
            return Firebase.likePost(postRef, this.meRef)
        return Firebase.likePost(this.currentPost, this.meRef)
    }

    /// Comments
    comments() {
        return Firebase.getComments(this.currentPost)        
    }

    comment() {
        return Firebase.getComment(this.currentComment)
    }

    newComment(text) {
        return Firebase.postComment(text, this.currentPost, this.meRef)
    }

    editComment(text) {
        return Firebase.editComment(this.currentComment, text)
    }
    
    deleteComment() {
        return Firebase.deleteComment(this.currentComment)
    }
}

// Std Firebase library
/// No context calls
/// All calls only accept refs, no ids
/// No calls to ContextualFirebase
/// Allowed to be called from anywhere
export class Firebase {

    static storageRef() {
        return firebase.storage().ref();
    }

    static uploadFile(user, path, meta) {
        const uid = user.uid
        if (!meta) {
            meta = {uid}
        } else {
            meta.uid = uid
        }

        /*const metadata = {
            contentType: 'image/jpg',
            customMetadata: meta
        }*/
        
        const uuid = Firebase.generateUUID()

        const fileRef = firebase.storage().ref(`/images/${uuid}.jpg`)
        return fileRef.putFile(path).then(uploadedFile => { // TODO: Something with the meta?
            return Promise.resolve({uploadedFile, fileRef, uid})
        })
    }

    static uploadFileGetLinkAndUser(user, path, meta) {
        return Firebase.uploadFile(path, meta).then(results => {
            return results.fileRef.getDownloadURL()
                .then(url => Promise.resolve({url, uid: results.uid}))
                .catch(e => Promise.reject(e.message))
        })
    }

    static signOut() {
        return firebase.auth().signOut()
    }

    static signIn(email, password) {
        return firebase.auth().signInAndRetrieveDataWithEmailAndPassword(email, password).then(creds => {
            return Promise.resolve(creds.user)
        }).catch(error => {
            return Promise.reject(error)
        })
    }

    static getUserData(userRef) {
        return userRef.get().then(doc => {
            const data = doc.data()
            return Promise.resolve(data)
        })
    }

    static getClassNames(userRef) {
        return Firebase.getClasses(userRef).then(classRefs => {
            const ids = []
            for (var i = 0; i < classRefs.length; i++) {
                const classRef = classRefs[i]
                ids.push(classRef.id)
            }
            return Promise.resolve(ids)
        })
    }

    static getClass(classRef) {
        return classRef.get().then(doc => {
            const data = doc.data()
            data.id = doc.id
            return Promise.resolve(data)
        })
    }

    static getClasses(userRef) {
        return userRef.get().then(userData => {
            return Promise.resolve(userData.data().classes)
        })
    }

    /// Gets post, userRef is optional
    static getPost(postRef, userRef) {
        return postRef.get().then(doc => {
            const data = doc
            const comments = Firebase.getComments(doc.ref)
            const likeCount = doc.ref.collection('likes').get()
            const myLikes = userRef ? doc.ref.collection('likes').doc(userRef.id).get() : []
            const classRef = doc.ref.parent.parent

            return Promise.all([data, Promise.resolve(comments), Promise.resolve(likeCount), Promise.resolve(myLikes), classRef])
        }).then(data => {
            const baseData = data[0]
            const comments = data[1]
            const likeCountData = data[2]
            const likeData = data[3]
            const classRef = data[4]

            const parsed = baseData.data()
            parsed.id = baseData.id
            parsed.comments = comments
            parsed.likeCount = likeCountData.size
            parsed.didLike = likeData.exists
            parsed.classRef = classRef
                        
            return Promise.resolve(parsed)
        })
    }

    /// userRef is optional, will get all posts from that user if specified
    static getPosts(classRef, userRef) {
        return classRef.collection("posts").get().then(query => {
            const posts = []
            query.forEach(q => {
                posts.push(Firebase.getPost(q.ref, userRef))
            })
            return Promise.all(posts)
        }).then(data => {
            return Promise.resolve(data)                
        })
    }

    static getAccountType(userRef) {
        return userRef.get().then(userDoc => {
            const data = userDoc.data()
            if (userDoc === undefined) {
                Firebase.signOut().then(
                    Promise.reject("Could not check account, signed out")
                )
            }

            let type = accountType.unregistered
            if (data.registered)
                type = accountType.student
            else if (data.isTeacher)
                type = accountType.teacher

            return Promise.resolve(type)
        })
    }

    static setUserData(userRef, userData) {
        return userRef.set(userData, { merge: true })
    }

    static completeAccountSetup(user, userRef, email, password) {
        return user.updateEmail(email)
            .then(() => {
                return user.updatePassword(password).then(() => {
                    // Flag the account as registered in the database                    
                    return userRef.set({ registered: true }, { merge: true })
                })
            })
            .catch(e => {
                return Promise.reject(e.code)
            })
    }

    static postComment(comment, postRef, userRef) {
        const comments = postRef.collection('comments')
        return comments.add({
            user: userRef,
            comment,
            dateTime: new Date()
        })
    }

    static getComment(commentRef) {
        return commentRef.get().then(comment => {
            return Promise.resolve(comment)
        })
    }

    static getComments(postRef) {
        const collectionRef = postRef.collection("comments")

        return collectionRef.get().then(query => {
            const comments = []
            const commentUsers = []
            query.forEach(comment => {
                comments.push(comment)
                commentUsers.push(comment.data().user.get())
            })
            return Promise.all([comments, Promise.all(commentUsers)])
        }).then(data => {
            const commentData = data[0]
            const userData = data[1]

            commentData.sort((x, y) => {
                return y.dateTime - x.dateTime
            })

            const parsedData = []
            for (let i = 0; i < commentData.length; i++) {
                const parsed = commentData[i].data()
                parsed.id = commentData[i].id
                parsed.userData = userData[i].data()
                parsedData.push(parsed)
            }
            return Promise.resolve(parsedData)
        })
    }

    static editComment(commentRef, newText) {
        return commentRef.set({ comment: newText }, { merge: true })
    }

    static deleteComment(commentRef) {
        return commentRef.delete()
    }

    static newPost(classRef, body, notify) {
        const posts = classRef.collection('posts')
        return posts.add({
            body,
            notify,
            dateTime: new Date()
        })
    }

    static likePost(postRef, userRef) {
        const likeRef = postRef.collection('likes').doc(userRef.id)

        return likeRef.get().then(doc => {
            const ref = doc.ref
            if (doc.exists) {
                // User already likes post, unlike
                return ref.delete().then(() => Promise.resolve({ didLike: false }))
            } else {
                return ref.set({user: userRef}).then(() => Promise.resolve({ didLike: true }))
            }
        })
    }

    // For use in admin app
    static updateUsername(user, firstName, lastName) {
        const userName = firstName.toLowerCase()[0] + lastName.toLowerCase()
        return user.updateProfile({displayName: userName, photoUrl: ""})
    }

    static updateProfileImage(user, profilePhoto) {
        return user.updateProfile({displayName: user.displayName, photoUrl: profilePhoto})
    }

    static authStateChanged() {
        return firebase.auth().onAuthStateChanged()
    }

    static getCurrentUser() {
        return firebase.auth().currentUser;
    }

    static generateUUID() { // Public Domain/MIT
        var d = new Date().getTime();
        if (typeof performance !== 'undefined' && typeof performance.now === 'function') {
            d += performance.now(); //use high-precision timer if available
        }
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
    }
}