import React from 'react'
import { Card, ScheduleCard } from '../components/card'
import TimeFormatter from './time'

import { Firebase } from './firebase'
import { contextualFirebase } from '../utils/store'

export default class Fetch {
    static getCards(cardClick) {
        return Fetch.getFormattedPosts().then(data => Fetch.getCardsFromData(data, cardClick))
    }

    static getCardsFromData(data, cardClick) {
        const postCards = []
        const scheduleCards = []

        for (var p = 0; p < data.postCards.length; p++) {
            const postCard = data.postCards[p]
            postCards.push(
                <Card
                    key={p}
                    onCardClick={() => cardClick(postCard.classId, postCard.postId)}
                    classId={postCard.classId}
                    postId={postCard.postId}
                    profileImage={postCard.profileImage}
                    username={postCard.username}
                    className={postCard.className}
                    dateTime={postCard.dateTime}
                    body={postCard.body}
                    didLike={postCard.didLike}
                    likeCount={postCard.likeCount}
                    comments={postCard.comments}
                    email={postCard.email} />
            )
        }

        for (var s = 0; s < data.scheduleCards.length; s++) {
            const scheduleCard = data.scheduleCards[s];
            scheduleCards.push(
                <ScheduleCard
                    key={s}
                    classId={scheduleCard.classId}
                    postId={scheduleCard.postId}
                    startTime={scheduleCard.startTime}
                    endTime={scheduleCard.endTime}
                    room={scheduleCard.room}
                    profileImage={scheduleCard.profileImage}
                    username={scheduleCard.username}
                    className={scheduleCard.className}
                    dateTime={scheduleCard.dateTime}
                    body={scheduleCard.body} />
            )
        }

        return { postCards, scheduleCards }
    }

    static formattedPost(post, classObj, teacherData) {
        const me = contextualFirebase.getState().me
        
        const name = `${teacherData.meta.firstName} ${teacherData.meta.lastName}`
        post.comments.sort((x, y) => {
            return y.dateTime - x.dateTime
        })
        for (let c of post.comments) {
            c.isMine = c.user.id === me.uid
        }

        return {
            classId: classObj.id,
            isMine: teacherData.id === me.uid,
            postId: post.id,
            comments: post.comments,
            didLike: post.didLike,
            likeCount: post.likeCount,
            profileImage: teacherData.profileImage,
            username: name,
            className: classObj.name,
            timestamp: post.dateTime,
            dateTime: TimeFormatter.getFormattedDate(post.dateTime),
            body: post.body,
            email: teacherData.meta.email
        }
    }

    static getFormattedPost() {
        return contextualFirebase.getState().post().then(post => {
            return Firebase.getClass(post.classRef).then(classObj => {
                return Firebase.getUserData(classObj.teacher).then(teacherData => {
                    teacherData.id = classObj.teacher.id

                    return Promise.resolve(Fetch.formattedPost(post, classObj, teacherData))
                })
            }) 
        })
    }

    static dateSort = (x, y) => {
        return y.timestamp - x.timestamp
    }

    static getFormattedPosts() {
        // Get classes and posts
        return contextualFirebase.getState().classes()
            .then(classRefs => {
                let postCards = []
                const scheduleCards = []

                for (var i = 0; i < classRefs.length; i++) {
                    const classRef = classRefs[i]
                    return Firebase.getClass(classRef).then(classObj => {
                        return Firebase.getUserData(classObj.teacher).then(teacherData => {
                            teacherData.id = classObj.teacher.id

                            return contextualFirebase.getState().posts(classRef).then(posts => {
                                // Iterate through each post in class
                                for (var p = 0; p < posts.length; p++) {
                                    const post = posts[p]
                                    postCards.push(Fetch.formattedPost(post, classObj, teacherData))
                                }
                                // Sort postCards
                                postCards.sort(Fetch.dateSort)
                                
                                // Push schedule card
                                const firstPost = postCards[0]
                                const name = `${teacherData.meta.firstName} ${teacherData.meta.lastName}`
                                scheduleCards.push({
                                    classId: classObj.id,
                                    postId: firstPost.id,
                                    startTime: TimeFormatter.getTime(classObj.startTime),
                                    endTime: TimeFormatter.getTime(classObj.endTime),
                                    room: classObj.room,
                                    profileImage: teacherData.profileImage,
                                    username: name,
                                    className: classObj.name,
                                    dateTime: firstPost.dateTime,
                                    body: firstPost.body
                                })
                                return Promise.resolve({postCards, scheduleCards})
                            })                            
                        })
                    })
                }
            })
    }
}