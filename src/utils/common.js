export default class Common {
    static instance = null;

    constructor() {
        this._firebase;
    }

    /**
     * @returns {Common}
     */
    static getInstance() {
        if (this.instance == null) {
            this.instance = new Common();
        }

        return this.instance;
    }

    getFirebase() {
        return this._firebase;
    }

    setFirebase(firebase) {
        this._firebase = firebase;
    }
}