export default class TimeFormatter {
    static getTime(date) {
        let minutes = date.getMinutes()
        if (minutes === 0)
            minutes = "00"
        else if (minutes % 10 === 0)
            minutes += "0"
        else if (minutes < 10)
            minutes = `0${minutes}`

        let hours = date.getHours()
        hours = hours > 12 ? hours - 12 : hours
        
        return `${(hours)}:${minutes}`
    }

    static getFormattedDate(date) {
        const months = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ]
        const days = ["Sun", "Mon", "Tues", "Wed", "Thurs", "Fri", "Sat"] // TODO: Was this week?
        let time = `${months[date.getMonth()]} ${date.getDate()}, ${this.getTime(date)}`

        const now = new Date()
        if (
            now.getFullYear() === date.getFullYear() &&
            now.getMonth() === date.getMonth() &&
            now.getDate() === date.getDate()
        ) {
            time = this.getTime(date)
        }
        return time
    }
}