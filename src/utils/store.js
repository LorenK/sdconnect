import { createStore } from 'redux'
import { ContextualFirebase } from './firebase'

export const Actions = {
	REMOVE: -1,
	ADD: 0,
	EDIT: 1
}

export const Context = {
	USER: 'user',
	SCHOOL: 'school',
	CLASS: 'class',
	POST: 'post',
	COMMENT: 'comment'
}

// refs store accepts:
/// -type (ADD/REMOVE)
/// -context (USER/SCHOOL/CLASS...)
/// -value
export const refs = createStore(
	(state = {}, action) => {
		switch(action.type) {
			case Actions.ADD:
				state[action.context] = action.value
				return state
			case Actions.REMOVE:
				state[action.context] = null
				return state;
			default:
				return state
		}
	}
)

export const contextualFirebase = createStore(
	(state, action) => {
		switch(action.type) {
			case Actions.ADD:
				state = new ContextualFirebase()
				return state
			case Actions.REMOVE:
				state = null
				return state
			default:
				return state
		}
	}
)