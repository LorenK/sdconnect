export default {
    "back": "arrow-back",
    "forward": "arrow-forward",
    "search": "search",
    "news": "newspaper",
    "upcoming": "calendar-clock",
    "settings": "settings",
    "heartEmpty": "heart-outline",
    "heart": "heart",
    "comment": "comment-text",
    "email": "email",
    "share": "share",
    "profile": "account",
    "notifications": "bell",
    "help": "help-circle",
    "logout": "logout",
    "camera": "camera",
    "delete": "delete"
}