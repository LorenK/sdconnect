// Define our color palate
export default {
    primary: "#2699FB",
    secondary: "#BCE0FD",
    background: "#F1F9FF",
    textPrimary: "#fff",
    textSecondary: "#B4B4B4",
    selectedIcon: "#fff",
    unselectedIcon: "#68B8FC",
}