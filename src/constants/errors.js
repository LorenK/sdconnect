export default {
    signInFailedGeneric: "Sorry, please check your username and password and try again",
    invalidEmail: "The email entered is invalid",
    accountDisabled: "Sorry, your account has been disabled, please see an administrator",
    noEmailSupplied: "Please enter your email",
    noPasswordSupplied: "Please enter a password",
    noPasswordConfirmSupplied: "Please re-enter a password",
    passwordConfirmationMismatch: "Passwords don't match",
}