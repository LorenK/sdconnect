// Define our font names
export default {
    arial: "Arial",
    arialBold: "Arial-BoldMT",
    arialItalic: "Arial-ItalicMT",
    roboto: "Roboto-Regular",
    robotoBold: "Roboto-Medium",
    robotoLight: "Roboto-Light",
    robotoThin: "Roboto-Thin",
    georgia: "Georgia",
    georgiaBold: "Georgia-Bold",
}