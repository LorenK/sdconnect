import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native'
export default class Form extends Component {
    constructor(props) {
        super(props)

        this.styles = StyleSheet.create({
            padding: {
                padding: 10
            }
        });
    }

    render() {
        let arr = []
        for (var i = 0; i < this.props.children.length; i++) {
            arr.push(
                <View key={i} style={this.styles.padding}>
                    {this.props.children[i]}
                </View>
            )
        }
        return (
            <View {...this.props}>
                {arr}
            </View>
        )
    }
}