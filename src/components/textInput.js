import React, { Component } from 'react'
import {
    View,
    Text,
    TextInput
} from 'react-native'
import styles from '../styles/textInput'
import textStyles from '../styles/textStyles'
import colors from '../constants/colors'
import PropTypes from 'prop-types'

export class STextInput extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isFocused: false
        }
    }
    
    render() {
        const isFocused = this.state.isFocused
        const { label, errorValue } = this.props
        return(
            <View>
                {label &&
                    <Text style={[styles.label, styles.sm]}>{label}</Text> }
                <TextInput
                    ref={tb => this.tb = tb}
                    {...this.props}
                    onFocus={this.focus}
                    onBlur={this.blur}
                    selectionColor={colors.secondary}
                    style={[textStyles.input, isFocused ? styles.baseInputFocused : styles.baseInput]}
                    placeholderTextColor={colors.secondary}
                    underlineColorAndroid="transparent" />
                {errorValue &&
                    <Text style={textStyles.error}>{errorValue}</Text>
                }
            </View>
        )
    }

    blur = () => {
        this.setState({isFocused: false})
    }

    focus = () => {
        this.setState({isFocused: true})
    }
}
STextInput.propTypes = {
    errorText: PropTypes.string,
    label: PropTypes.string
}

export class ClearTextInput extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isFocused: false
        }
    }
    
    render() {
        return(
            <TextInput
                {...this.props}
                selectionColor={colors.secondary}
                style={[textStyles.input, styles.clearInput, this.props.customStyle]}
                placeholderTextColor={colors.secondary}
                underlineColorAndroid="transparent" />
        )
    }
}
ClearTextInput.propTypes = {
    customStyle: PropTypes.object
}