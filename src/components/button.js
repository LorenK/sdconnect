import React, { Component } from 'react'
import { Platform, View, Text, TouchableNativeFeedback, TouchableHighlight, TouchableOpacity } from 'react-native'
import colors from '../constants/colors'
import { buttonStyles, cardButtonStyles } from '../styles/button'
import textStyles from '../styles/textStyles'
import { BasicIcon } from "./icon"
import PropTypes from 'prop-types'

export class Button extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { text, icon, noFill, onPress, disabled, style } = this.props
        let wrapStyle = [style, noFill ? buttonStyles.outline : buttonStyles.fill]
        
        const content =
            <View style={Platform.OS === 'android' ? wrapStyle : undefined}>
                {text &&
                    <Text style={[noFill ? buttonStyles.textNoFill : buttonStyles.text, textStyles.button]}>
                        {text}
                    </Text>
                }
                {icon &&
                    <BasicIcon style={[noFill ? buttonStyles.iconNoFill : buttonStyles.icon]} name={icon} size={20} />
                }
            </View>

        if (disabled) {
            return (content)
        }
        
        let touchable =
            <TouchableOpacity style={wrapStyle} onPress={onPress}>
                {content}
            </TouchableOpacity>
        if (Platform.OS === 'android') {
            touchable =
                <TouchableNativeFeedback
                    onPress={onPress}>
                    {content}
                </TouchableNativeFeedback>
        }

        return(touchable)
    }
}
Button.propTypes = {
    text: PropTypes.string,
    icon: PropTypes.string,
    noFill: PropTypes.bool,
    name: PropTypes.string,
    onPress: PropTypes.func.isRequired
}

export class CardButton extends Component {
    render() {
        const { icon, name, value, onPress } = this.props
        const content =
            <View style={cardButtonStyles.container}>
                <BasicIcon comName={icon} size={20} color={colors.primary} style={{paddingRight: 16}} />          
                <Text style={[cardButtonStyles.text, textStyles.pb]}>{name}</Text>
                <View style={cardButtonStyles.floatRight}>
                    <Text style={cardButtonStyles.textSecondary}>{value}</Text>
                </View>
            </View>

        let touchable =
            <TouchableHighlight style={{width: "100%"}} onPress={onPress}>{content}</TouchableHighlight>
        if (Platform.OS === 'android')
            touchable = <TouchableNativeFeedback onPress={onPress}>{content}</TouchableNativeFeedback>

        return(touchable)
    }
}
CardButton.propTypes = {
    icon: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    onPress: PropTypes.func
}