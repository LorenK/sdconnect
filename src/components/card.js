import React, { Component } from 'react'
import {
    View,
    Text,
    Image,
    Switch,
    Alert,
    TouchableWithoutFeedback,
    Picker,
    ActivityIndicator
} from 'react-native'
import SIcon from './icon'
import BasicIcon from './icon'
import styles from '../styles/card'
import textStyles from '../styles/textStyles'
import icons from '../constants/icons'
import colors from '../constants/colors'
import { STextInput } from '../components/textInput'
import { Button } from '../components/button'
import PropTypes from 'prop-types'

import accountType from '../constants/accountType'
import { refs, contextualFirebase } from '../utils/store'

export class NewPostCard extends Component {
    constructor(props) {
        super(props)

        this.state = {
            disabled: false,
            content: '',
            notify: false,
            classNames: ['COSC121'] // TODO: Fetch from remote
        }
    }

    clear() {
        this.setState({content: '', notify: false, disabled: false})
    }

    render() {
        const defaultPadding = 12
        const { disabled } = this.state
        const { classes, extraStyles } = this.props

        return(
            <View style={[styles.container, extraStyles]}>
                <View style={[{ padding: defaultPadding }]}>
                    <Text style={[styles.actionText, textStyles.pb]}>NEW POST</Text>
                </View>
                <View style={{padding: defaultPadding, paddingTop: 0}}>
                    <STextInput
                        editable={!disabled}
                        multiline={true}
                        placeholder={"content"}
                        keyboardType={"email-address"}
                        onChangeText={(content) => this.setState({content})}
                        value={this.state.content}
                    />

                    <Picker
                        selectedValue={this.state.language}
                        onValueChange={(itemValue, itemIndex) => this.setState({language: itemValue})}>
                        <Picker.Item label="Java" value="java" />
                        <Picker.Item label="JavaScript" value="js" />
                    </Picker>
                    
                    <View style={{flexDirection: "row"}}>
                        <Text>Notify?</Text>
                        <Switch
                            disabled={disabled}
                            onValueChange={(notify) => this.setState({notify})}
                            value={this.state.notify}
                            thumbTintColor={colors.primary}
                            onTintColor={colors.secondary}
                        />
                    </View>
                    <View style={{
                        flexDirection: 'row',
                        justifyContent: "center",
                        alignSelf: "center",
                        alignItems: "center"}}>
                        <Button disabled={disabled} style={{flex: 1}} icon={icons.forward} onPress={this.onPost} />
                    </View>
                </View>
            </View>
        )
    }

    onPost = () => {
        const { onSubmit } = this.props
        this.setState({disabled: true})

        onSubmit('cosc121', this.state.content, this.state.notify)
    }
}

export class ScheduleCard extends Component {
    render() {
        const { startTime, endTime, room, username, className, dateTime, body } = this.props
        return(
            <View>
                <View style={styles.scheduleCard}>
                    <Text style={[textStyles.scheduleHead, styles.head]}>{startTime} - {endTime}</Text>
                    <Text style={[textStyles.scheduleSubhead, styles.head]}>{className} / {room}</Text>
                </View>
                <Card username={username} className={className} dateTime={dateTime} body={body} showControls={false} />
            </View>
        )
    }
}
ScheduleCard.propTypes = {
    startTime: PropTypes.string.isRequired,
    endTime: PropTypes.string.isRequired,
    room: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
    className: PropTypes.string.isRequired,
    dateTime: PropTypes.string.isRequired,
    body: PropTypes.string.isRequired
}

export class Card extends Component {
    constructor(props) {
        super(props)
        
        const { didLike, likeCount, classId, postId, comments } = this.props
        this.state = {
            loading: false,
            comments,
            likeCount: likeCount,
            liked: didLike
        }

        this.firebase = contextualFirebase.getState()

        this.currentClass = refs.getState().school.class(classId)
        this.currentPost = this.currentClass.post(postId)
    }

    hideLoader() {
        this.setState({loading: false})
    }

    showLoader() {
        this.setState({loading: true})
    }

    componentDidMount() {
        // Subscribe for changes in the current post
        refs.subscribe(() => {
            const post = refs.getState().post
            if (post) {
                if (this.currentPost.id == post.id) {                    
                    // Refresh current card
                    this.showLoader()

                    this.firebase.post().then(postData => {
                        this.hideLoader()

                        this.setState({
                            comments: postData.comments,
                            likeCount: postData.likeCount,
                            liked: postData.didLike
                        })
                    })
                }
            }
        })
    }

    onCardClicked = () => {
        const { onCardClick } = this.props
        if (onCardClick)
            onCardClick()
    }

    render() {
        let { profileImage, username, className, dateTime, body, email, showControls } = this.props
        if (showControls === undefined)
            showControls = true

        const likeIcon = this.state.liked ? icons.heart : icons.heartEmpty

        const defaultPadding = 12
        const iconSize = 20

        let profileImageElm =
            <View style={styles.profileEmpty}>
                <BasicIcon comName={icons.profile} size={20} color={colors.primary} />
            </View>
        if (profileImage)
            profileImageElm =
                <Image style={styles.profilePhoto} source={{uri: profileImage}} />

        return(
            <View style={styles.container}>
                {this.state.loading &&
                    <ActivityIndicator style={{flex: 1}} size={"large"} color={colors.primary} />
                }

                {!this.state.loading &&
                    <TouchableWithoutFeedback onPress={this.onCardClicked}>
                        <View>
                            <View style={[styles.row, { padding: defaultPadding }]}>
                                    {profileImageElm}
                                    <View style={{ padding: 8 }}>
                                        <Text style={[styles.actionText, textStyles.pb]}>{username}</Text>
                                        <Text style={[styles.actionText, textStyles.sm]}>{className}</Text>
                                    </View>
                                <View style={[styles.center, styles.floatRight]}>
                                    <Text style={[styles.textBody, textStyles.xsmi]}>{dateTime}</Text>
                                </View>
                            </View>
                            <View style={{padding: defaultPadding, paddingTop: 0}}>
                                <Text style={[styles.textBody, textStyles.sm]}>{body}</Text>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                }
                {!this.state.loading && showControls &&
                    <View style={[styles.row, {padding: defaultPadding}]}>
                        <View style={{flex: 1}}>
                            <View style={styles.row}>
                                <View style={[styles.row, styles.center]}>
                                    <SIcon onPress={this.toggleLike} style={{paddingRight: 4}} comName={likeIcon} size={iconSize} color={colors.primary} />
                                    <Text style={[styles.actionText, textStyles.pb]}>{this.state.likeCount}</Text>
                                </View>
                                <View style={[styles.row, styles.center, {paddingLeft: defaultPadding + 2}]}>
                                    <SIcon style={{paddingRight: 4}} comName={icons.comment} size={iconSize} color={colors.primary} />
                                    <Text style={[styles.actionText, textStyles.pb]}>{this.state.comments.length}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <SIcon onPress={() => this.composeEmail(email)} name={icons.email} size={iconSize} color={colors.primary} />
                        </View>
                    </View>
                }
            </View>
        )
    }

    toggleLike = () => {
        const { liked, likeCount } = this.state
        
        // Show the icon right away, remove if remote call fails
        this.setState({ liked: !liked, likeCount: liked ? likeCount - 1 : likeCount + 1 })
        
        this.firebase.likePost(this.currentPost.doc()).then(data => data).catch(() => {
            this.setState({ liked, likeCount })
            this.showErrorAlert("Like failed, check network connection and try again later")
        })

        // TODO: This is too slow for UI updates, only call onPageLoad, not every like
        // Ensure teachers can't like their own content
        /*
        Firebase.getAccountType().then(account => {
            if (account === accountType.student) {
                
            }            
        })
        */
    }

    composeEmail = (address) => {
        console.warn(address)
    }

    showErrorAlert(msg) {
        Alert.alert('Woops', msg)
    }
}
Card.propTypes = {
    username: PropTypes.string.isRequired,
    className: PropTypes.string.isRequired,
    dateTime: PropTypes.string.isRequired,
    body: PropTypes.string.isRequired,
    comments: PropTypes.array,
    email: PropTypes.string,
    showControls: PropTypes.bool
}