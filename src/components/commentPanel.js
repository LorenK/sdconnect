import React, { Component } from 'react'
import { STextInput } from './textInput'
import { View, Text, Image, TouchableOpacity, BackHandler } from 'react-native'
import cardStyles from '../styles/card'
import textStyles from '../styles/textStyles'
import icons from '../constants/icons'
import { Button } from '../components/button'

import { ContextController } from '../utils/firebase'
import { refs, contextualFirebase } from '../utils/store'

export default class CommentPanel extends Component {
    constructor(props) {
        super(props)

        this.comment = props.comment
        this.state = {
            showCommentBox: true,
            editing: false,
            isMine: this.comment.isMine,
            commentId: this.comment.id,
            comment: this.comment.comment,
            userData: this.comment.userData
        }

        BackHandler.addEventListener('hardwareBackPress', () => {
            if (this.state.editing) {
                // Disable editing
                this.setState({ editing: false })

                return true
            }
            return false
        })

        refs.subscribe(() => {
            const currentComment = refs.getState().comment
            if (currentComment) {
                const isEditing = currentComment.id === this.state.commentId
                this.setState({ editing: isEditing })
            }
        })
       
        // contextualFirebase.getState().editComment(this.state.comment)
    }

    componentWillUnmount() {
        // TODO: This throws an error
        //refs.unsubscribe()
    }

    commentLongHold(commentId) {
        if (this.state.isMine) {
            // Set redux state
            ContextController.setComment(commentId)
            if (this._component.tb) {
                // Show keyboard
                this._component.tb.focus()
            }
        }
    }

    render() {
        const { commentId, comment, editing, userData } = this.state
        return(
            <TouchableOpacity onLongPress={() => this.commentLongHold(commentId)}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Image style={{width: 32, height: 32, borderRadius: 16, marginLeft: 12}} source={{uri: userData.profileImage}} />

                    {editing &&
                        <View style={{flex: 1, flexDirection: 'row', margin: 12}}>
                            <View style={{flex: 1}}>
                                <STextInput
                                    ref={component => this._component = component}
                                    onChangeText={(comment) => this.setState({comment})}
                                    value={comment}
                                />
                            </View>
                            <Button style={{marginLeft: 12}} icon={icons.delete} onPress={() => this.delete(commentId)} />
                            <Button style={{marginLeft: 12}} icon={icons.forward} onPress={() => this.saveEdits(commentId)} />
                        </View>
                    }
                    {!editing &&
                        <View style={{margin: 12}}>
                            <Text style={[textStyles.pb, cardStyles.actionText]}>@{userData.meta.username}</Text>
                            <Text style={[textStyles.p, cardStyles.actionText]}>{comment}</Text>
                        </View>
                    }
                </View>
            </TouchableOpacity>
        )
    }

    saveEdits() {
        contextualFirebase.getState().editComment(this.state.comment).then(() => {
            this.setState({editing: false})

            // Hide comment panel
            ContextController.setComment(undefined)
        })
    }

    delete() {
        contextualFirebase.getState().deleteComment().then(() => {
            this.setState({editing: false})

            // Hide comment panel
            ContextController.setComment(undefined)
        })
    }
}