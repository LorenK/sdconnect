import React, { Component } from 'react'
import {
    View,
    Text,
    TouchableOpacity,
	StyleSheet
} from 'react-native'
import textStyles from '../styles/textStyles'
import colors from '../constants/colors'

const styles = StyleSheet.create({
	tagContainer: {
		flexDirection: 'row'
	},
	tag: {
		marginRight: 12,
		backgroundColor: colors.secondary
	},
	tagSelected: {
		marginRight: 12,
		backgroundColor: colors.primary
	},
	tagText: {
		color: colors.textPrimary,
		padding: 8
	}
})

export class TagContainer extends Component {
	constructor(props) {
		super(props)
		this.state = {
			selectedIndex: 0
		}
	}

	onTagSelected(selectedIndex) {
		console.warn(selectedIndex)
		this.setState({selectedIndex})
	}

	render() {
		const { items } = this.props
		const tags = []
		for (var i = 0; i < items.length; i++) {
			const tag = items[i]
			tags.push(<Tag key={i} value={tag} selected={this.state.selectedIndex === i} onPress={() => this.onTagSelected(i)} />)
		}

		return(
			<View style={styles.tagContainer}>
				{tags}
			</View>
		)
	}
}

export class Tag extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		const { index, onPress, value, selected } = this.props
		return(
			<TouchableOpacity onPress={onPress}>
				<View style={selected ? styles.tagSelected : styles.tag}>
					<Text style={[styles.tagText, textStyles.p]}>{value}</Text>
				</View>
			</TouchableOpacity>
		)
	}

	onTagPressed = (onPress) => {
		console.warn(this.props.index)
		this.setState({ selected: true })
		// onPress()
	}
}