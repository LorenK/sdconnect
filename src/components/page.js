import colors from "../constants/colors"
import icons from "../constants/icons"

import { loginStyles, contentStyles, plainStyles } from "../styles/page"
import textStyles from "../styles/textStyles"

import React, { Component } from 'react'
import {
    View,
    Text,
    ActivityIndicator
} from 'react-native'
import { NavigationActions } from 'react-navigation'
import SIcon from "./icon"
import Pager from "./pager"
import Swiper from 'react-native-swiper'
import PropTypes from 'prop-types'
import { contextualFirebase } from '../utils/store'

import { Firebase } from '../utils/firebase'

export class PlainLayout {
    constructor(title, showBackButton) {
        this.title = title

        this.styles = plainStyles
        this.showSearchButton = false
        this.showBackButton = showBackButton
    }
}

export class FormLayout extends PlainLayout {
    constructor(title, showBackButton) {
        super(title)

        this.styles = loginStyles
        this.showPager = false
        this.showSearchButton = false
        this.showBackButton = showBackButton !== undefined ? showBackButton : true
    }
}

export class ContentLayout extends PlainLayout {
    constructor(title, showBackButton) {
        super(title)

        this.styles = contentStyles
        this.showPager = true
        this.showSearchButton = true
        this.showBackButton = showBackButton !== undefined ? showBackButton : false
    }
}

export class BottomButton {
    constructor(text, onPress) {
        this.text = text
        this.onPress = onPress
    }
}

//// TODO: THIS CAUSES ERROR
/*class AuthPage extends Page {
    constructor(props, layout) {
        super(props, layout)
        this.componentDidMount = this.componentDidMount.bind(this)
    }

    componentDidMount() {
        this.contextController.setUser(Firebase.getCurrentUser())
        this.contextController.setSchool('rss') // TODO, get from Remote
        
        super.getFirebase().subscribeToNotifications()
    }
}*/

export class Page extends Component {
    constructor(props, layout) {
        super(props)
        const { navigate } = this.props.navigation

        this.layout = layout
        this.navigate = navigate
        this.state = {
            pageIndex: 0,
            title: typeof(this.layout.title) === "string" ? this.layout.title : this.layout.title[0],
            showLoader: false,
            refreshing: false
        }
    }

    getFirebase() {
        return contextualFirebase.getState()
    }

    showLoader() {
        this.setState({
            showLoader: true
        })
    }

    hideLoader() {
        this.setState({
            showLoader: false
        })
    }

    componentWillUnmount() { }

    componentDidMount() { }

    renderContent() {
        throw new Error('renderContent not implemented');
    }

    goBack() {
        this.onBackPressed()
    }

    onBackPressed() {
        const backAction = NavigationActions.back()
        this.props.navigation.dispatch(backAction)
    }

    hardNavigate(page) {
        const resetAction = NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: page }),
            ],
        })
        this.props.navigation.dispatch(resetAction)
    }

    onSearchPressed = () => {
        this.navigate("Search", {  })
    }

    render() {
        const styles = this.layout.styles
        const showPager = this.layout.showPager

        const iconSize = 24

        let content =
            <View style={{flex: 1}}>
                {this.renderContent()}
            </View>
        if (showPager) {
            content =
                <View style={{flex: 1}}>
                    <Swiper
                        ref={(component) => { this.swiper = component; }}
                        loop={false}
                        showsPagination={false}
                        index={0}
                        onIndexChanged={(index) => this.onIndexChanged(index)}>
                        {this.renderContent()}
                    </Swiper>
                    <Pager index={this.state.pageIndex} itemPressed={(i) => this.swiperItemPressed(i)} />
                </View>
        }
        
        return(
            <View style={styles.container}>                
                <View style={[styles.topBar, styles.topBarColor]}>
                    {this.layout.showBackButton &&
                        <SIcon onPress={() => this.onBackPressed()} style={[styles.topBarItem, styles.topBarItemColor, styles.left]} name={icons.back} size={iconSize} />
                    }
                    {!this.layout.showBackButton && <View style={[styles.topBarItemNoText]} /> }
                    <Text style={[styles.topBarItem, styles.topBarItemColor, styles.center, textStyles.pb]}>{this.state.title}</Text>
                    {this.layout.showSearchButton &&
                        <SIcon onPress={this.onSearchPressed} style={[styles.topBarItem, styles.topBarItemColor, styles.right]} name={icons.search} size={iconSize} />
                    }
                    {!this.layout.showSearchButton && <View style={[styles.topBarItemNoText]} /> }
                </View>
                
                {this.state.showLoader &&
                    <ActivityIndicator style={{flex: 1}} size={"large"} color={colors.primary} />
                }
                {!this.state.showLoader && content}
            </View>
        )
    }

    onIndexChanged(index) {
        this.setState({
            pageIndex: index,
            title: this.layout.title[index]
        })
    }

    swiperItemPressed = (targetIndex) => {
        var index = targetIndex - this.state.pageIndex
        this.swiper.scrollBy(index, true)
    }
}