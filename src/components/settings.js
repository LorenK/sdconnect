import React from 'react'
import icons from "../constants/icons"
import colors from "../constants/colors"
import {
    View,
    TouchableWithoutFeedback,
    Image
} from 'react-native'
import { BasicIcon } from '../components/icon'
import { CardButton } from "../components/button"
import { StyleSheet } from 'react-native'
import PropTypes from 'prop-types'
import { Firebase } from '../utils/firebase'

const styles = StyleSheet.create({
    container: {
        justifyContent: "center",
        alignItems: "center"
    },
    profileFilled: {
        marginBottom: 24,
        width: 140,
        height: 140,
        borderRadius: 70,
    },
    profileEmpty: {
        marginBottom: 24,
        backgroundColor: colors.secondary,
        width: 140,
        height: 140,
        borderRadius: 70,
        justifyContent: "center",
        alignItems: "center"
    }
})
export default class Settings extends React.Component {
    render() {
        const { contentMargins, navigate, profileImage, username } = this.props

        let imageElm = 
            <View style={styles.profileEmpty}>
                <BasicIcon comName={icons.profile} size={52} color={colors.primary} />
            </View>
        if (profileImage) {
            imageElm = <Image style={styles.profileFilled} source={{uri: profileImage}} />
        }

        return(
            <View style={[styles.container, {marginLeft: contentMargins, marginRight: contentMargins}]}>
                <TouchableWithoutFeedback onPress={() => this.props.navigate("ProfileImage")}>
                    {imageElm}
                </TouchableWithoutFeedback>

                <CardButton icon={icons.profile} name={"Username"} value={`@${username}`} />
                <CardButton icon={icons.notifications} name={"Notifications"} value={"All"} />
                <CardButton icon={icons.help} name={"Help"} value={"Questions?"} />
                <CardButton icon={icons.logout} name={"Logout"} value={""} onPress={this.logout} />
            </View>
        )
    }

    logout = () => {
        Firebase.signOut().then(() => {
            this.props.navigate("Splash")
        })
    }
}
Settings.propTypes = {
    navigate: PropTypes.func.isRequired,
    contentMargins: PropTypes.number
}