import React, { Component } from 'react'
import { View } from 'react-native'
import icons from '../constants/icons'
import colors from '../constants/colors'
import pagerStyles from '../styles/pager'
import SIcon from './icon'
import PropTypes from 'prop-types';

export default class Pager extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        let { index, itemPressed } = this.props

        function selectedColor(targetIndex) {
            return targetIndex == index ? colors.selectedIcon : colors.unselectedIcon
        }

        const size = 20;
        return (
            <View style={pagerStyles.container}>
                <SIcon comName={icons.news} size={size} color={selectedColor(0)} onPress={() => itemPressed(0)} style={pagerStyles.pagerItem} />
                <SIcon comName={icons.upcoming} size={size} color={selectedColor(1)} onPress={() => itemPressed(1)} style={pagerStyles.pagerItem} />
                <SIcon comName={icons.settings} size={size} color={selectedColor(2)} onPress={() => itemPressed(2)} style={pagerStyles.pagerItem} />
            </View>
        )
    }
}
Pager.propTypes = {
    index: PropTypes.number.isRequired,
    itemPressed: PropTypes.func.isRequired
}