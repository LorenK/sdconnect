import React from 'react'
import { TouchableWithoutFeedback, View } from 'react-native'
import MaterialIcon from 'react-native-vector-icons/MaterialIcons'
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons'

export class BasicIcon extends React.Component {
    render() {
        const { name, comName, size, color, backgroundColor, style } = this.props

        let icon
        if (comName) {
            icon = <MaterialCommunityIcon name={comName} size={size} color={color} style={style} />
        } else {
            icon = <MaterialIcon name={name} size={size} color={color} style={style} />
        }

        return (icon)
    }
}

export default class SIcon extends React.Component {
    render() {
        let { onPress, name, comName, size, color, backgroundColor, style } = this.props

        let icon
        if (comName) {
            icon = <MaterialCommunityIcon name={comName} size={size} color={color} style={style} />
        } else {
            icon = <MaterialIcon name={name} size={size} color={color} style={style} />
        }

        let content = icon
        if (backgroundColor) {
            const backgroundSize = size + 20
            content =
                <View style={{
                    backgroundColor: backgroundColor,
                    borderRadius: backgroundSize,
                    alignItems: 'center',
                    justifyContent: 'center',
                    width: backgroundSize,
                    height: backgroundSize}}>
                    {icon}
                </View>
        }

        return (
            <TouchableWithoutFeedback onPress={onPress}>
                {content}
            </TouchableWithoutFeedback>
        )
    }
}