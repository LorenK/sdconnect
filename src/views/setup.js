import React, { Component } from 'react'
import {
    View,
    Text,
} from 'react-native'
import { STextInput } from '../components/textInput'
import icons from '../constants/icons'
import colors from '../constants/colors'
import errors from '../constants/errors'
import { loginStyles } from '../styles/page'
import styles from '../styles/textInput'
import textStyles from '../styles/textStyles'
import Form from '../components/form'
import { Button } from '../components/button'

import { Page, FormLayout } from "../components/page"
import { Firebase } from '../utils/firebase'

class Setup extends Page {
    constructor(props) {
        super(props, new FormLayout("SETUP", false))
        this.renderContent = this.renderContent.bind(this)
        this.componentDidMount = this.componentDidMount.bind(this)
    }

    componentDidMount() {
        this.setState({
            email: '',
            emailError: undefined,
            password: '',
            passwordError: undefined,
            confirmPassword: '',
            confirmPasswordError: undefined,
            genericError: undefined
        })
    }
    
    renderContent() {        
        const {
            email, emailError,
            password, passwordError,
            confirmPassword, confirmPasswordError,
            genericError
        } = this.state

        return(
            <View>
                <Form style={{padding: 24}}>
                    <STextInput
                        label={"Email"}
                        onChangeText={(email) => this.setState({email})}
                        value={email}
                        errorValue={emailError}
                    />
                    <STextInput
                        label={"New Password"}
                        onChangeText={(password) => this.setState({password})}
                        value={password}
                        secureTextEntry={true}
                        errorValue={passwordError}
                    />
                    <STextInput
                        label={"Confirm Password"}
                        onChangeText={(confirmPassword) => this.setState({confirmPassword})}
                        value={confirmPassword}
                        secureTextEntry={true}
                        errorValue={confirmPasswordError}
                    />
                    <Text style={textStyles.error}>{genericError}</Text>
                    
                    <View style={loginStyles.buttonView}>
                        <Button style={{flex: 1}} icon={icons.forward} onPress={this.validateForm} />
                    </View>
                </Form>
                <View style={{ backgroundColor: "#000" }}>
                    <Text>LOG OUT</Text>
                </View>
            </View>
        )
    }

    signOut = () => Firebase.signOut().then(this.showLogin())

    clearErrors() {
        this.setState({
            emailError: undefined,
            passwordError: undefined,
            confirmPasswordError: undefined,
            genericError: undefined
        })
    }

    validateForm = () => {
        this.showLoader()
        this.clearErrors()

        let errorCount = 0
        const { email, password, confirmPassword } = this.state

        // TODO: Cleanup
        if (email === '') {
            this.setState({
                emailError: errors.noEmailSupplied
            })
            errorCount++
        }
        if (password === '') {
            this.setState({
                passwordError: errors.noPasswordSupplied
            })
            errorCount++
        }
        if (confirmPassword === '') {
            this.setState({
                confirmPasswordError: errors.noPasswordConfirmSupplied
            })
            errorCount++
        }
        if (password !== confirmPassword) {
            this.setState({
                genericError: errors.passwordConfirmationMismatch
            })
            errorCount++
        }

        if (errorCount != 0)
            this.hideLoader()
        else
            this.updateAccount()
    }

    updateAccount() {
        const { email, password } = this.state
        this.getFirebase().completeAccountSetup(email, password).then(() => {
            this.showHome()
        }).catch(e => {
            // We had an error, sign us out and go to login page

            this.hideLoader()
            this.setState({
                genericError: e
            })
            Firebase.signOut().then(() => {
                this.showLogin()
            })
        })
    }

    showLogin() {
        this.hardNavigate("Login")
    }

    showHome() {
        this.hardNavigate("Home")
    }
}
module.exports = Setup