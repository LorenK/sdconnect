import React from 'react'
import { loginStyles } from '../styles/page'
import colors from "../constants/colors"
import icons from "../constants/icons"
import { Page, FormLayout } from "../components/page"
import { Button } from '../components/button'
import SIcon from '../components/icon'
import { View, Image } from 'react-native'
import { RNCamera } from 'react-native-camera'

export default class ProfileImage extends Page {
    constructor(props) {
        super(props, new FormLayout("PROFILE"))
        this.renderContent = this.renderContent.bind(this)
        this.componentDidMount = this.componentDidMount.bind(this)
    }

    componentDidMount() {
        this.setState({
            cameraReady: false
        })
    }

    renderContent() {
        const { imageData } = this.state
        const showSkip = false // Can be overriden

        return (
            <View style={{flex: 1, padding: 24}}>
                <View style={{flex: 1, alignContent: "center", alignSelf: "center", alignItems: "center"}}>
                    {!imageData &&
                        <RNCamera
                            ref={(cam) => {
                                this.camera = cam;
                            }}
                            ratio={"4:3"}
                            onCameraReady={this.onCameraReady}
                            style={{width: 270, height: 360}}
                            type={RNCamera.Constants.Type.front} />
                    }
                    {imageData &&
                        <Image source={{uri: imageData}} style={{width: 270, height: 360}} />
                    }
                    <View style={{flexDirection: "row", alignContent: "center", alignSelf: "center", alignItems: "center", justifyContent: "center", padding: 12}}>
                        <SIcon
                            comName={imageData ? icons.delete : icons.camera}
                            size={40}
                            onPress={imageData ? this.trashPhoto : this.takePicture}
                            color={imageData ? colors.primary : colors.textPrimary}
                            backgroundColor={imageData ? colors.textPrimary : colors.primary} />
                    </View>
                </View>
                <View style={loginStyles.buttonView}>
                    {showSkip &&
                        <Button style={{flex: 1, marginRight: 8}} text={"SKIP"} noFill={true} onPress={this.skip} /> }
                    <Button style={{flex: 1, marginLeft: 8}} icon={icons.forward} onPress={this.uploadAndContinue} />
                </View>
            </View>
        )
    }

    onCameraReady = () => this.setState({ cameraReady: true })

    onFacesDetected = (faceObj) => {
        console.warn(faceObj)
    }

    onFaceDetectionError = (faceObj) => {
        console.warn(faceObj)
    }

    trashPhoto = () => {
        this.setState({ imageData: undefined })
    }

    takePicture = () => {
        this.camera.takePictureAsync({
            quality: 0.5
        }).then(image => this.setState({ imageData: image.uri }))
    }

    skip = () => {

    }

    uploadAndContinue = () => {
        this.showLoader()
        this.getFirebase().uploadFileGetLinkAndUser(this.state.imageData)
            .then((results) => {
                // Update the db
                const data = { profileImage: results.url }
                this.getFirebase().setMyData(data)
                    .then(() => {
                        this.goBack()
                    })
                    .catch(e => {
                        // Show error
                        this.hideLoader()
                    })
            })
            .catch(e => {
                // Show error
                this.hideLoader()
            })
    }
}