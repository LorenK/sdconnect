import React from 'react'
import {
    View,
    StyleSheet,
    FlatList,
} from 'react-native'
import icons from '../constants/icons'
import colors from '../constants/colors'
import { plainStyles, contentStyles } from '../styles/page'
import { Page, PlainLayout } from "../components/page"
import { Card } from '../components/card'
import { BasicIcon } from '../components/icon'
import { ClearTextInput } from '../components/textInput'

import Fetch from "../utils/fetch"

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    searchbarContainer: {
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: colors.textPrimary,
        marginBottom: 24,
        padding: 24,
        paddingTop: 0
    },
    searchTextInput: {
        paddingLeft: 12,
        width: "100%"
    },
})
class Search extends Page {
    constructor(props) {
        super(props, new PlainLayout("", true))
        this.renderContent = this.renderContent.bind(this)
        this.componentDidMount = this.componentDidMount.bind(this)
    }

    componentDidMount() {
        this.setState({
            refreshing: true,
            postCards: []
        })

        this.getPostData()
    }

    getPostData() {
        this.setState({ refreshing: true})
        // Get classes and posts
        Fetch.getFormattedPosts().then(data => {
            this.setState({
                refreshing: false,
                postCards: data.postCards
            })
        })
    }

    getIndicesOf(searchStr, str) {
        var searchStrLen = searchStr.length;
        if (searchStrLen == 0) {
            return [];
        }
        var startIndex = 0, index, indices = [];
        while ((index = str.indexOf(searchStr, startIndex)) > -1) {
            indices.push(index);
            startIndex = index + searchStrLen;
        }
        return indices;
    }

    search = (searchText) => {
        if (!this.state.refreshing) {
            const search = searchText.toLowerCase()
            const data = this.state.postCards
            const occurrences = []
            
            // Search, username, body, className
            for (let i = 0; i < data.length; i++) {
                const post = data[i]

                const usernameOccurrences = this.getIndicesOf(search, post.username.toLowerCase())
                const classNameOccurrences = this.getIndicesOf(search, post.className.toLowerCase())
                const bodyOccurrences = this.getIndicesOf(search, post.body.toLowerCase())
                const rank = usernameOccurrences.length + classNameOccurrences.length + bodyOccurrences.length
                
                if (rank > 0)
                    occurrences.push({id: i, usernameOccurrences, classNameOccurrences, bodyOccurrences, rank})
            }
            occurrences.sort((x, y) => {
                return x.rank > y.rank
            })

            const filteredCardData = { postCards: [], scheduleCards: [] }
            for (var s = 0; s < occurrences.length; s++) {
                const occurrence = occurrences[s]
                filteredCardData.postCards.push(data[occurrence.id])
            }

            const filteredCards = Fetch.getCardsFromData(filteredCardData, this.onCardClick).postCards         
            this.setState({ filteredCards })
        }
    }

    onCardClick = (classId, postId) => {
        this.navigate('FullPost', { classId, postId })
    }

    renderContent() {
        return(
            <View style={styles.container}>
                <View style={styles.searchbarContainer}>
                    <BasicIcon size={32} color={colors.primary} name={icons.search} />
                    <View style={styles.searchTextInput}>
                        <ClearTextInput
                            disableFullscreenUI={true}
                            autoCapitalize={'none'}
                            blurOnSubmit={true}
                            autoFocus={true}
                            onChangeText={this.search}
                            placeholder="search"
                            customStyle={{fontSize: 40}} />
                    </View>
                </View>

                <View style={{backgroundColor: colors.background}}>
                    <FlatList
                        style={contentStyles.contentMargins}
                        showsHorizontalScrollIndicator={false}
                        showsVerticalScrollIndicator={false}
                        data={ this.state.filteredCards }
                        refreshing={this.state.refreshing}
                        renderItem={ ( {item} ) => item }
                    />
                </View>
            </View>
        )
    }
}
module.exports = Search