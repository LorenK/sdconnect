import React from 'react'
import {
    View,
    Text,
} from 'react-native'
import { STextInput } from '../components/textInput'
import icons from '../constants/icons'
import errors from '../constants/errors'
import colors from '../constants/colors'
import { loginStyles } from '../styles/page'
import textStyles from '../styles/textStyles'
import Form from '../components/form'
import { Button } from '../components/button'
import { NavigationActions } from 'react-navigation'

import { Page, FormLayout } from "../components/page"
import { Firebase, ContextController } from '../utils/firebase'

class Login extends Page {
    constructor(props) {
        super(props, new FormLayout("LOGIN"))
        this.renderContent = this.renderContent.bind(this)
        this.componentDidMount = this.componentDidMount.bind(this)
    }

    componentDidMount() {
        this.setState({
            email: '',
            emailError: undefined,
            password: '',
            passwordError: undefined,
            genericError: undefined
        })
    }
    
    renderContent() { 
        const {
            email, emailError,
            password, passwordError,
            genericError
        } = this.state

        return(
            <Form style={{padding: 24}}>
                <STextInput
                    label={"Email"}
                    keyboardType={"email-address"}
                    onChangeText={(email) => this.setState({email})}
                    value={email}
                    errorValue={emailError}
                />
                <STextInput
                    label={"Password"}
                    onChangeText={(password) => this.setState({password})}
                    value={password}
                    secureTextEntry={true}
                    errorValue={passwordError}
                />
                <Text style={textStyles.error}>{genericError}</Text>

                <View style={loginStyles.buttonView}>
                    <Button style={{flex: 1}} icon={icons.forward} onPress={this.validateForm} />
                </View>
            </Form>
        )
    }

    clearErrors() {
        this.setState({
            emailError: undefined,
            passwordError: undefined,
            genericError: undefined
        })
    }

    validateForm = () => {
        this.showLoader()
        this.clearErrors()

        let errorCount = 0
        const { email, password } = this.state

        // TODO: Cleanup
        if (email === '') {
            this.setState({
                emailError: errors.noEmailSupplied
            })
            errorCount++
        }
        if (password === '') {
            this.setState({
                passwordError: errors.noPasswordSupplied
            })
            errorCount++
        }

        if (errorCount != 0)
            this.hideLoader()
        else
            this.logIn()
    }

    logIn() {
        const { email, password } = this.state
        Firebase.signIn(email, password).then(user => {
            ContextController.setUser(user)
            this.getFirebase().getAccountType().then(accountType => {
                switch(accountType) {
                    case 0:
                        this.showSetup()
                    break
                    case 1:
                        this.showHome()
                    break
                    case 2:
                        this.showTeacher()
                    break
                }
            })
        }).catch(e => {
            this.hideLoader()
            let error = errors.signInFailedGeneric
            switch (e.code) {
                case "auth/invalid-email":
                    error = errors.invalidEmail
                    break
                case "auth/user-disabled":
                    error = errors.accountDisabled
                    break
            }
            this.setState({
                genericError: error
            })
        })
    }

    showSetup() {
        this.hardNavigate("Setup")
    }

    showHome = () => {
        this.hardNavigate("Home")
    }

    showTeacher = () => {
        this.hardNavigate("TeacherHome")
    }
}
module.exports = Login