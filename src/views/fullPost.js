import React from 'react'
import { View, Text, Image, ScrollView, TouchableWithoutFeedback, BackHandler } from 'react-native'
import { Page, PlainLayout } from "../components/page"
import Fetch from '../utils/fetch'
import SIcon from '../components/icon'
import BasicIcon from '../components/icon'
import cardStyles from '../styles/card'
import textStyles from '../styles/textStyles'
import icons from '../constants/icons'
import colors from '../constants/colors'
import { STextInput } from '../components/textInput'
import { Button } from '../components/button'
import CommentPanel from '../components/commentPanel'

import { ContextController } from '../utils/firebase'
import { refs } from '../utils/store'

// This post is fullscreen, TODO: Stop passing class and post ID's and assemble refs instead
export default class FullPost extends Page {
    constructor(props) {
        super(props, new PlainLayout("", true))
        this.renderContent = this.renderContent.bind(this)
        this.componentDidMount = this.componentDidMount.bind(this)
        this.componentWillUnmount = this.componentWillUnmount.bind(this)
        
        const { classId, postId } = this.props.navigation.state.params
        this.classId = classId
        this.postId = postId

        ContextController.setClass(this.classId)
        ContextController.setPost(this.postId)

        refs.subscribe(() => {
            const currentComment = refs.getState().comment
            this.setState({ showCommentPanel: currentComment.id ? false : true })
        })
    }

    componentDidMount() {
        this.showLoader()

        Fetch.getFormattedPost().then(data => {
            data.title = data.className.toUpperCase()
            this.setState(data)
            this.hideLoader()
        })
        this.getFirebase().getMyData().then(profileData => {
            this.setState({
                myProfileImage: profileData.profileImage
            })
        })

        this.setState({ showCommentPanel: true })
    }
    
    componentWillUnmount() {
        // This will trigger any subs for the post, we may have made changes
        ContextController.setPost(this.postId)  
    }

    renderContent() {
        let { profileImage, username, className, dateTime, body, email, comments, myProfileImage, myComment } = this.state

        const likeIcon = this.state.didLike ? icons.heart : icons.heartEmpty

        const defaultPadding = 12
        const iconSize = 20

        let profileImageElm =
            <View style={cardStyles.profileEmpty}>
                <BasicIcon comName={icons.profile} size={20} color={colors.primary} />
            </View>
        if (profileImage)
            profileImageElm =
                <Image style={cardStyles.profilePhoto} source={{uri: profileImage}} />

        const commentElms = []
        let commentCount = 0
        if (comments) {
            commentCount = comments.length
            for (let i = 0; i < commentCount; i++) {
                const comment = comments[i]
                commentElms.push(
                    <CommentPanel key={comment.id} comment={comment} />
                )
            }
        }
        
        return(
            <View style={{flex: 1}}>
                <View style={[cardStyles.container, { paddingTop: 0}]}>
                    <View style={[cardStyles.row, { padding: defaultPadding, paddingTop: 0 }]}>
                            {profileImageElm}
                            <View style={{ padding: 8 }}>
                                <Text style={[cardStyles.actionText, textStyles.pb]}>{username}</Text>
                                <Text style={[cardStyles.actionText, textStyles.sm]}>{className}</Text>
                            </View>
                        <View style={[cardStyles.center, cardStyles.floatRight]}>
                            <Text style={[cardStyles.textBody, textStyles.xsmi]}>{dateTime}</Text>
                        </View>
                    </View>
                    <View style={{padding: defaultPadding, paddingTop: 0}}>
                        <Text style={[cardStyles.textBody, textStyles.sm]}>{body}</Text>
                    </View>

                    <View style={[cardStyles.row, {padding: defaultPadding}]}>
                        <View style={{flex: 1}}>
                            <View style={cardStyles.row}>
                                <View style={[cardStyles.row, cardStyles.center]}>
                                    <SIcon onPress={this.toggleLike} style={{paddingRight: 4}} comName={likeIcon} size={iconSize} color={colors.primary} />
                                    <Text style={[cardStyles.actionText, textStyles.pb]}>{this.state.likeCount}</Text>
                                </View>
                                <View style={[cardStyles.row, cardStyles.center, {paddingLeft: defaultPadding + 2}]}>
                                    <SIcon style={{paddingRight: 4}} comName={icons.comment} size={iconSize} color={colors.primary} />
                                    <Text style={[cardStyles.actionText, textStyles.pb]}>{commentCount}</Text>
                                </View>
                            </View>
                        </View>
                        <View style={cardStyles.row}>
                            <SIcon onPress={() => this.composeEmail(email)} name={icons.email} size={iconSize} color={colors.primary} />
                        </View>
                    </View>
                </View>
                
                <ScrollView style={{flex: 1}}>
                    {commentElms}
                </ScrollView>

                {this.state.showCommentPanel &&
                    <View style={[{flexDirection: 'row', alignItems: 'center'}]}>
                        <Image style={{width: 32, height: 32, borderRadius: 16, margin: 12 }} source={{uri: myProfileImage}} />
                        
                        <View style={{flex: 1}}>
                            <STextInput
                                placeholder={"comment"}
                                onChangeText={(myComment) => this.setState({myComment})}
                                value={myComment}
                            />
                        </View>
                        <Button style={{margin: 12}} icon={icons.forward} onPress={this.validateComment} />
                    </View>
                }
            </View>            
        )
    }

    validateComment = () => {
        const { myComment } = this.state
        if (myComment && myComment !== '') {
            this.getFirebase().postComment(myComment).then(() => {
                this.setState({myComment: ''})
                this.getComments()
            })
        }
    }

    getComments() {
        this.getFirebase().getComments().then(comments => {
            this.setState({comments})
        })
    }

    // TODO: Repeating code..
    toggleLike = () => {
        const { didLike, likeCount } = this.state
        
        // Show the icon right away, remove if remote call fails
        this.setState({ didLike: !didLike, likeCount: didLike ? likeCount - 1 : likeCount + 1 })
                
        this.getFirebase().likePost().then(data => data).catch(() => {
            this.setState({ didLike, likeCount })
            this.showErrorAlert("Like failed, check network connection and try again later")
        })

        // TODO: This is too slow for UI updates, only call onPageLoad, not every like
        // Ensure teachers can't like their own content
        /*
        this.getFirebase().getAccountType().then(account => {
            if (account === accountType.student) {
                
            }            
        })
        */
    }
}