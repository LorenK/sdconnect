import React from 'react'
import {
    View,
    FlatList,
} from 'react-native'
import { contentStyles } from '../styles/page'

import { NewPostCard } from '../components/card'
import { Page, ContentLayout } from "../components/page"
import Settings from "../components/settings"
import Fetch from "../utils/fetch"

class TeacherHome extends Page {
    constructor(props) {
        super(props, new ContentLayout(["NEWS", "SCHEDULE", "SETTINGS"]))
        this.renderContent = this.renderContent.bind(this)
        this.componentDidMount = this.componentDidMount.bind(this)
    }

    componentDidMount() {
        this.showLoader()
        this.setState({
            refreshing: true,
            postCards: [],
            scheduleCards: []
        })

        this.getPostData()
        this.getProfileData()
    }

    getPostData() {
        this.setState({ refreshing: true});
        // Get classes and posts
        return Fetch.getCards().then(data => {
            data.postCards.unshift(
                <NewPostCard
                    key={-1}
                    ref={(newPost) => {
                        this.newPost = newPost
                    }}
                    onSubmit={this.post}
                />
            )
            this.setState({
                refreshing: false,
                postCards: data.postCards,
                scheduleCards: data.scheduleCards
            })

            // Check if we have data, replace loader with page specific loader
            if (data.postCards.length > 0 && data.scheduleCards.length > 0)
                this.hideLoader()

            return data
        })
    }

    getProfileData() {
        this.getFirebase().getMyData().then(data => {
            this.setState({
                myProfileImage: data.profileImage,
                username: data.meta.username
            })
        })
    }

    post = (classId, content, notify) => {
        if (classId && content)
            this.firebase.newPost(classId, content, notify).then(() => this.newPost.clear())
        else {
            this.newPost.clear()
        }
    }
    
    renderContent() {
        const arr = [
            <View key={0}>
                <FlatList
                    style={contentStyles.contentMargins}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    data={ this.state.postCards }
                    refreshing={this.state.refreshing}
                    onRefresh={() => this.getPostData() }
                    renderItem={ ( {item} ) => item }
                />
            </View>,
            <View key={1}>
                <FlatList
                    style={contentStyles.contentMargins}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    data={ this.state.scheduleCards }
                    renderItem={ ( {item} ) => item }
                />
            </View>,
            <Settings profileImage={this.state.myProfileImage} username={this.state.username} navigate={this.navigate} contentMargins={16} key={2} />
        ]

        return(arr)
    }
}
module.exports = TeacherHome