import React, { Component } from 'react'
import { View, Text, Image, ActivityIndicator } from 'react-native'
import colors from '../constants/colors'
import accountType from '../constants/accountType'
import textStyles from '../styles/textStyles'
import styles from '../styles/splash'
import { Button } from '../components/button'
import { NavigationActions } from 'react-navigation'
import { Page, PlainLayout } from "../components/page"

import firebase from 'react-native-firebase'
import { Actions, contextualFirebase } from '../utils/store'
import { ContextController } from '../utils/firebase'

class Splash extends Page {
    constructor(props) {
        super(props, new PlainLayout(""))
        this.renderContent = this.renderContent.bind(this)
        this.componentDidMount = this.componentDidMount.bind(this)

        this.state = {
            loading: true,
            accountType: -1
        }
    }

    componentDidMount() {
        // Setup our context
        ContextController.setSchool('rss') // TODO, get from Remote
        contextualFirebase.dispatch({type: Actions.ADD})

        const { navigate } = this.props.navigation
        this.navigate = navigate

        this.authSubscription = firebase.auth().onAuthStateChanged(user => {
            if (user) {
                // Set context
                ContextController.setUser(user)
                this.getFirebase().getAccountType().then(accountType => {
                    this.setState({
                        loading: false,
                        accountType: accountType
                    })
                })
            } else {
                this.setState({
                    loading: false,
                    accountType: -1
                })
            }
        })
    }
    
    componentWillUnmount() {
        this.authSubscription()
    }
    
    renderContent() {
        // The application is initialising, TODO: Show loader
        if (this.state.loading) {
            return (<ActivityIndicator style={{flex: 1}} size={"large"} color={colors.primary} />)
        }
        switch(this.state.accountType) {
            case 0:
                this.showSetup()
            break
            case 1:
                this.showHome()
            break
            case 2:
                this.showTeacherHome()
            break
        }
        
        return(
            <View style={{flex: 1, backgroundColor: colors.textPrimary}}>
                <View style={{flex: 1}}>
                    <Image source={require("../img/logo.png")} resizeMode={"contain"} style={styles.image} />
                    <Text style={[textStyles.h1, styles.header]}>Excepteur sint occaecat</Text>
                    <Text style={[textStyles.h2, styles.subheader]}>Ut labore et dolore roipi mana aliqua. Enim adeop minim veeniam nostruklad</Text>
                </View>
                <View>
                    <View style={styles.buttonView}>
                        <Button style={{flex: 1}} text={"LOGIN"} onPress={this.showLoginPage} />
                    </View>
                </View>
            </View>
        )
    }
    
    hardNavigation(page) {
        const resetAction = NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: page }),
            ],
        })
        // TODO Causing warning
        this.props.navigation.dispatch(resetAction)
    }

    showHome() {
        this.hardNavigation("Home")
    }

    showTeacherHome() {
        this.hardNavigation("TeacherHome")
    }

    showSetup() {
        this.hardNavigation("Setup")
    }

    showLoginPage = () => {
        this.navigate("Login")
    }
}
module.exports = Splash