import React from 'react'
import {
    View,
    FlatList,
} from 'react-native'
import { contentStyles } from '../styles/page'

import { Page, ContentLayout } from "../components/page"
import Settings from "../components/settings"
import Fetch from "../utils/fetch"

class Home extends Page {
    constructor(props) {
        super(props, new ContentLayout(["NEWS", "SCHEDULE", "SETTINGS"]))
        this.renderContent = this.renderContent.bind(this)
        this.componentDidMount = this.componentDidMount.bind(this)
    }

    componentDidMount() {
        this.showLoader()
        this.setState({
            refreshing: true,
            postCards: [],
            scheduleCards: []
        })

        this.getPostData()
        this.getProfileData()
    }

    onCardClick = (classId, postId) => {
        this.navigate('FullPost', { classId, postId })
    }

    getPostData() {
        this.setState({ refreshing: true});
        // Get classes and posts

        return Fetch.getCards(this.onCardClick).then(data => {
            this.setState({
                refreshing: false,
                postCards: data.postCards,
                scheduleCards: data.scheduleCards
            })

            // Check if we have data, replace loader with page specific loader
            if (data.postCards.length > 0 && data.scheduleCards.length > 0)
                this.hideLoader()

            return data
        })
    }

    getProfileData() {
        this.getFirebase().getMyData().then(data => {
            this.setState({
                myProfileImage: data.profileImage,
                username: data.meta.username
            })
        })
    }
    
    renderContent() {        
        const arr = [
            <View key={0}>
                <FlatList
                    style={contentStyles.contentMargins}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    data={ this.state.postCards }
                    refreshing={this.state.refreshing}
                    onRefresh={() => this.getPostData() }
                    renderItem={ ( {item} ) => item }
                />
            </View>,
            <View key={1}>
                <FlatList
                    style={contentStyles.contentMargins}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    data={ this.state.scheduleCards }
                    renderItem={ ( {item} ) => item }
                />
            </View>,
            <Settings profileImage={this.state.myProfileImage} username={this.state.username} navigate={this.navigate} contentMargins={16} key={2} />
        ]

        return(arr)
    }
}
module.exports = Home